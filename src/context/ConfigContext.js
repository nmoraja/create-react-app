import { createContext } from 'react';
import config from '../data/config.json';

const ConfigContext = createContext({});

export const ConfigProvider = ({ children }) => {
  return (
    <ConfigContext.Provider
      value={{
        config,
      }}
    >
      {children}
    </ConfigContext.Provider>
  );
};

export default ConfigContext;
