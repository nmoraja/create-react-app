import { createContext, useState, useEffect } from 'react';

const isWindowAvailable = typeof window !== 'undefined';

const getInnerWidth = () => (isWindowAvailable ? window.innerWidth : undefined);
const getInnerHeight = () =>
  isWindowAvailable ? window.innerHeight : undefined;

const ViewportContext = createContext({});

export const ViewportProvider = ({ children }) => {
  const [viewportSize, setViewportSize] = useState({
    width: getInnerWidth(),
    height: getInnerHeight(),
  });

  const handleViewportResize = () => {
    setViewportSize({
      width: getInnerWidth(),
      height: getInnerHeight(),
    });
  };

  useEffect(() => {
    window.addEventListener('resize', handleViewportResize);
    return () => window.removeEventListener('resize', handleViewportResize);
  }, []);

  return (
    <ViewportContext.Provider
      value={{
        viewportSize,
      }}
    >
      {children}
    </ViewportContext.Provider>
  );
};

export default ViewportContext;
