import { createContext } from 'react';
import data from '../data/data.json';

const DataContext = createContext({});

export const DataProvider = ({ children }) => {
  const { menu, pages } = data;
  return (
    <DataContext.Provider
      value={{
        menu,
        pages,
      }}
    >
      {children}
    </DataContext.Provider>
  );
};

export default DataContext;
