import { ViewportProvider } from '../../context/ViewportContext';
import { ScrollProvider } from '../../context/ScrollContext';
import {ScrollButton} from '../scrollButton/ScrollButton';
import { Header } from '../header/Header';
import { Footer } from '../footer/Footer';

export const Layout = ({ children }) => {
  return (
    <ViewportProvider>
      <ScrollProvider>
        <Header />
            <main className="main">{children}</main>
        <Footer />
        <ScrollButton />
      </ScrollProvider>
    </ViewportProvider>
  );
};
