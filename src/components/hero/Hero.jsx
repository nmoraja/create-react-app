import bg from './bg.png';
import './Hero.scss';

export const Hero = () => {
    return (
        <div className="hero" style={{backgroundImage: `url(${bg})`}}>
            <div className="container">
                <div className="hero__wrapper">
                    <div className="hero__img">
                        <img src="/images/hero.png" alt="Hero" />
                    </div>                      
                    <div className="hero__text">
                        <h1 className="hero__heading">
                            Create Amazing Landing Page With <span>CODER</span>
                        </h1>
                        <p className="lead hero__lead">
                        Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione sequinesciunt.
                        </p>
                        <div className="hero__buttons">
                            <a href="/" className="btn">Get Started</a>
                        </div>
                    </div>
                  
                </div>
            </div>
        </div>
    )
}
