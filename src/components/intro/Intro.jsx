import './Intro.scss';

const Intro = () => {
    return (
        <section className="section intro">
            <div className="container">
                <div className="intro__wrapper">
                    <h2 className="intro__heading"><span>We Are Here </span>For Made Your Idea</h2>
                    <p className="intro__paragraph">
                        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Fugit at inventore tempore dicta eligendi. Dolorem consectetur et natus? Odio alias totam excepturi et iste sequi nobis ea nulla voluptatem sunt?
                    </p>
                </div>
            </div>
        </section>
    )
}

export default Intro
