import { useContext } from 'react';
import ScrollContext from '../../context/ScrollContext';
import { KeyboardArrowUp } from '@material-ui/icons';
import './ScrollButton.scss';

export const ScrollButton = () => {
  const { clientWindow } = useContext(ScrollContext);

  const clickHandler = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    });
  };

  return (
    <button
      className={`scrollBtn ${
        clientWindow.pageYOffset > 300 ? 'is-visible' : ''
      }`}
      onClick={clickHandler}
    >
      <KeyboardArrowUp />
    </button>
  );
};
