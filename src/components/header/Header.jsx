import { useContext, useState } from 'react';
import ScrollContext from '../../context/ScrollContext';
import {Menu, Close} from '@material-ui/icons'

import './Header.scss';

export const Header = () => {
    const { clientWindow } = useContext(ScrollContext);

    const [menuOpen, setMenuOpen] = useState(false);

    const menuToggleHandler = () => {
        setMenuOpen(p => !p);
    };

    return (
        <header className={`header ${ clientWindow.pageYOffset ? 'sticky' : ''}`}>
            <div className="container">
                <div className="header__wrapper">
                    <div className="header__logo">
                        <a href="/">
                            <img src="/images/logo/logo.png" alt="Logo" />
                        </a>
                    </div>
                    <nav className="header__nav">
                        <ul className="header__nav__list">
                            <li className="header__nav__item">
                                <a className="header__nav__link is-active" href="/">Home</a>
                            </li>
                            <li className="header__nav__item">
                                <a className="header__nav__link" href="/">About</a>
                            </li>
                        </ul>
                    </nav>
                    <div className="header__buttons">
                        <a href="/" className="btn">Contact Us</a>
                    </div>
                    <div className="header__toggle">
                        {!menuOpen ? (
                        <Menu onClick={menuToggleHandler} />
                        ) : (
                        <Close onClick={menuToggleHandler} />
                        )}                    
                    </div>
                </div>
            </div>
        </header>
    )
}
