import {Facebook, Twitter, Instagram} from '@material-ui/icons';
import bg from './bg.png';
import './Footer.scss';

export const Footer = () => {
    const socials = [
        {
            href: '/',
            icon: <Facebook />
        },
        {
            href: '/',
            icon: <Twitter />
        },
        {
            href: '/',
            icon: <Instagram />
        }        
    ]
    return (
        <footer className="section footer" style={{backgroundImage: `url(${bg})`}}>
            <div className="container">
                <div className="footer__wrapper">
                    <div className="footer__start">
                        <div className="footer__logo">
                            <img src="/images/logo/white-logo.png" alt="Logo" />
                        </div>
                        <p className="lead footer__desc">Voluptatem eius eaque placeat incidunt excepturi perferendis aut! Harum animi officia alias.</p>

                        <ul className="footer__social">
                            {socials.map((social,  index) => (
                                <li key={index}>
                                    <a href={social.href}>
                                        {social.icon}
                                    </a>
                                </li>
                            ))}
                        </ul>
                    </div>
                    <div className="footer__end">
                        <div className="footer__end__wrapper">
                            <div className="footer__menu">
                                <h5 className="footer__menu__heading">Customer</h5>
                                <ul className="footer__menu__list">
                                    <li className="footer__menu__item">
                                        <a href="/" className="footer__menu__link">Works</a>
                                    </li>
                                </ul>
                            </div>
                            <div className="footer__menu">
                                <h5 className="footer__menu__heading">Customer</h5>
                                <ul className="footer__menu__list">
                                    <li className="footer__menu__item">
                                        <a href="/" className="footer__menu__link">Works</a>
                                    </li>
                                </ul>
                            </div>
                            <div className="footer__menu">
                                <h5 className="footer__menu__heading">Customer</h5>
                                <ul className="footer__menu__list">
                                    <li className="footer__menu__item">
                                        <a href="/" className="footer__menu__link">Works</a>
                                    </li>
                                </ul>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}
