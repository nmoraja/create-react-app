import Masonry from 'react-masonry-css';
import {SRLWrapper} from 'simple-react-lightbox'

import './Portfolio.scss';

export const Portfolio = () => {
    return (
        <section className="section portfolio">
            <div className="container">
                <div className="section__header">
                    <h2>Portfolio</h2>
                </div>                
                <SRLWrapper>
                    <Masonry breakpointCols={3} className="portfolio__grid" columnClassName="portfolio__grid__column">
                        {[1,2,3,4,5,6].map(img => (
                            <a className="portfolio__item" href={`/images/portfolio/${img}.jpg`} key={img}>
                                <img className="portfolio__img" src={`/images/portfolio/thumbs/${img}.jpg`} alt="#" />
                            </a>
                        ))}
                    </Masonry>
                </SRLWrapper>
            </div>
        </section>
    );
}