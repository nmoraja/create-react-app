import SimpleReactLightbox from 'simple-react-lightbox';
import { Layout } from './components/layout/Layout';
import Home from './pages/Home';

import './App.scss';

function App() {
  return (
    <SimpleReactLightbox>
      <Layout>
        <Home />
      </Layout>
    </SimpleReactLightbox>
  );
}

export default App;
