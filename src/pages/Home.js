import { Hero } from '../components/hero/Hero';
import { Portfolio } from '../components/portfolio/Portfolio';
import Intro from '../components/intro/Intro';

const Home = () => {
  return (
    <>
      <Hero />
      <Intro />
      <Portfolio />
    </>
  );
};

export default Home;
